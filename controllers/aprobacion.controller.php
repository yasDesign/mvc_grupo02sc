<?php 

/**
* 
*/
class aprobacionController extends Controlador
{
	
	function __construct()
	{
		parent::__construct();
		
    }
    
    function admin(){
        $this->view->estilo=$this->getEstilo();		
		$this->view->render('aprobacion/admin');
    }

	function index(){
		
		$this->view->estilo=$this->getEstilo();		
		$this->view->render('aprobacion/index');
	}	

	public function guardar(){
		$this->model->fecha = $_POST['fecha'];
        $this->model->autoridad = $_POST['autoridad'];
        echo $this->model->Registrar($this->model);
        
		//$this->view->render('categoria/admin');
	}

	public function listarJSON(){
		echo $this->model->ListarJSON();
	}	
	
}
 ?>