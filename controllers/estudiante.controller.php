<?php

/**
 *
 */
class estudianteController extends Controlador
{

    function __construct()
    {
        parent::__construct();

    }

    function admin(){
        $this->view->estilo=$this->getEstilo();
        //$this->view->contador=$this->model->visitar();
        //$this->view->tramite=$this->getLastTramite();
        //$this->view->proyecto=$this->getLastProyecto();
        //$this->view->publicacion=$this->getLastPublicacion();
        //echo "controller<br>";

        $this->view->render('estudiante/admin');
    }

    function index(){

        $this->view->estilo=$this->getEstilo();
        //$this->view->contador=$this->model->visitar();
        //$this->view->tramite=$this->getLastTramite();
        //$this->view->proyecto=$this->getLastProyecto();
        //$this->view->publicacion=$this->getLastPublicacion();
        //echo "controller<br>";

        $this->view->render('estudiante/index');
    }

    public function Guardar(){
        if(isset($_POST['id'])){
            $this->model->id=$_POST['id'];
        }
        $this->model->nombre = $_POST['nombre'];
        $this->model->apellido = $_POST['apellido'];
        $this->model->nacionalidad = $_POST['nacionalidad'];
        $this->model->fechanacimiento = $_POST['fechanacimiento'];
        $this->model->lugarnacimiento = $_POST['lugarnacimiento'];
        $this->model->cedula = $_POST['cedula'];
        $this->model->calle = $_POST['calle'];
        $this->model->ciudad = $_POST['ciudad'];
        $this->model->telefono = $_POST['telefono'];
        $this->model->correo = $_POST['correo'];
        $this->model->contrasena = $_POST['contrasena'];
        $this->model->sexo = $_POST['sexo'];
        $this->model->tipo = $_POST['tipo'];

        if($this->model->id > 0){
            echo $this->model->Actualizar($this->model);
        }else{
            echo $this->model->Registrar($this->model);
        }

        //require_once 'views/estudiante/index.php';
    }

    public function eliminar(){

        $id=$_POST['id'];

        echo $this->model->Eliminar($id);

    }

    public function listarJSON(){
        echo $this->model->ListarJSON();
    }

}