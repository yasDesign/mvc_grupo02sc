<?php 

/**
* 
*/
class inicioController extends Controlador
{
	
	function __construct()
	{
		parent::__construct();
		
	}

	function index(){
		
		 //$this->view->estilo=$this->getEstilo();
		 //$this->view->contador=$this->model->visitar();	
		//$this->view->render('inicio/index');
        $this->view->render2();
	}

	public function login(){
		$this->view->estilo=$this->getEstilo();
		$this->view->toLogin=1;
		$this->view->render('inicio/login');
	}
		
	public function registro(){
		$this->view->estilo=$this->getEstilo();
		$this->view->render('inicio/registro');
	}

	public function guardar(){
		$this->model->nombre = $_POST['nombre'];
        $this->model->apellido = $_POST['apellido'];
        $this->model->nacionalidad = $_POST['nacionalidad'];
        $this->model->fechanacimiento = $_POST['fechanacimiento'];
        $this->model->lugarnacimiento = $_POST['lugarnacimiento'];
        $this->model->cedula = $_POST['cedula'];
        $this->model->calle = $_POST['calle'];
        $this->model->ciudad = $_POST['ciudad'];
        $this->model->telefono = $_POST['telefono'];
        $this->model->correo = $_POST['correo'];
		$this->model->contrasena = $_POST['contrasena'];
		$this->model->RegistrarEstudiante($this->model);
		$this->view->estilo=$this->getEstilo();
		$this->view->render('inicio/registro');
	}

	public function dashboard(){
		$this->view->estilo=$this->getEstilo();
		$this->view->render('dashboard/index');
	}

	public function iniciar(){
		
		$nro=$this->model->Cantidad($_POST['correo'],$_POST['contrasena']);
		//print_r($nro->cantidad);
		if($nro->cantidad > 0){
			//echo $nro->cantidad;
			$usu=$this->model->getUsuario($_POST['correo'],$_POST['contrasena']);
			
			//print_r($usu);

			//rint_r($usu['tipo']);
			Sesion::init();
			Sesion::set('correo',$usu['correo']);
			Sesion::set('nombre',$usu['nombre']);
			Sesion::set('tipo',$usu['tipo']);
			Sesion::set('id',$usu['id']);
			//header('location:../inicio');
			//exit();
			echo 1;
		}else{
			echo 0;
			//echo "usuario no existe";
		}
		
	}

	public function cerrar(){
		Sesion::init();
		Sesion::destroy();
		header('location:../inicio');
			exit();
	}

	public function busqueda(){
		$this->view->buscar=$_POST['buscar'];
		$this->view->estilo=$this->getEstilo();
		$this->view->render('inicio/busqueda');
	}
	
}
 ?>