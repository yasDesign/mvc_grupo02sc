<?php 

/**
* 
*/
class inscripcionController extends Controlador
{
	
	function __construct()
	{
		parent::__construct();
		
    }
    
    function admin(){
	    $this->view->estilo=$this->getEstilo();
		 //$this->view->contador=$this->model->visitar();		
		 //$this->view->tramite=$this->getLastTramite();
		 //$this->view->proyecto=$this->getLastProyecto();
		 //$this->view->publicacion=$this->getLastPublicacion();
		 //echo "controller<br>";
		
		$this->view->render('inscripcion/admin');
    }

	function index(){
		
		 $this->view->estilo=$this->getEstilo();
		 //$this->view->contador=$this->model->visitar();		
		 //$this->view->tramite=$this->getLastTramite();
		 //$this->view->proyecto=$this->getLastProyecto();
		 //$this->view->publicacion=$this->getLastPublicacion();
		 //echo "controller<br>";
		
		$this->view->render('inscripcion/index');
	}	

	public function guardar(){
		if(isset($_POST['id'])){
			$this->model->id=$_POST['id'];
		}
		$this->model->fecha = $_POST['fecha'];
		$this->model->detalle = $_POST['detalle'];
        $this->model->idusuario = $_POST['idusuario'];
		$this->model->idtranscriptor = $_POST['idtranscriptor'];

        if($this->model->id > 0){
			echo $this->model->Actualizar($this->model);
		}else{
			echo $this->model->Registrar($this->model);
		}
	}

	public function eliminar(){
		
		$id=$_POST['id'];
		
		echo $this->model->Eliminar($id);
	}

    public function listarInscriciopnEstudiante(){
        Sesion::init();
        $id=Sesion::get('id');
        echo $this->model->obtenerInsc($id);
    }

    public function inscrEstudian(){
        $this->view->estilo=$this->getEstilo();

        $this->view->render('inscripcion/estudiante');

    }
	public function listarJSON(){
		echo $this->model->ListarJSON();
	}	
	
}
 ?>