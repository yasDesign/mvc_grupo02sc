<?php 

/**
* 
*/
class moduloController extends Controlador
{
	
	function __construct()
	{
		parent::__construct();
		
    }
    
    function admin(){
        $this->view->estilo=$this->getEstilo();
		$this->view->render('modulo/admin');
    }

	function index(){
		
		$this->view->estilo=$this->getEstilo();		
		$this->view->render('modulo/index');
	}	

	public function guardar(){
		if(isset($_POST['id'])){
			$this->model->id=$_POST['id'];
		}
		$this->model->nombre = $_POST['nombre'];
		$this->model->descripcion = $_POST['descripcion'];
        $this->model->docente = $_POST['docente'];
        $this->model->estado = $_POST['estado'];
        $this->model->combalidacion = $_POST['combalidacion'];


        if($this->model->id > 0){
			echo $this->model->Actualizar($this->model);
		}else{
			echo $this->model->Registrar($this->model);
		}
	}

	public function eliminar(){
		
		$id=$_POST['id'];
		
		echo $this->model->Eliminar($id);
		
	}

	public function listarJSON(){
		echo $this->model->ListarJSON();
	}	
	
}
 ?>