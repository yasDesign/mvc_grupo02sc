<?php 

/**
* 
*/
class requisitoController extends Controlador
{
	
	function __construct()
	{
		parent::__construct();
		
    }
    
    function admin(){
        $this->view->estilo=$this->getEstilo();	
		$this->view->render('requisito/admin');
    }

	function index(){
		
		$this->view->estilo=$this->getEstilo();
		$this->view->render('requisito/index');
	}	

	public function guardar(){
		if(isset($_POST['id'])){
			$this->model->id=$_POST['id'];
		}
		$this->model->nombre = $_POST['nombre'];
		$this->model->descripcion = $_POST['descripcion'];
		if($this->model->id > 0){
			echo $this->model->Actualizar($this->model);
		}else{
			echo $this->model->Registrar($this->model);
		}
	}

	public function eliminar(){
		
		$id=$_POST['id'];
		
		echo $this->model->Eliminar($id);
		
	}

	public function listarJSON(){
		echo $this->model->ListarJSON();
	}	
	
}
 ?>