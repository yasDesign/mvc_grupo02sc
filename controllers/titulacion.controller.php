<?php 

/**
* 
*/
class titulacionController extends Controlador
{
	
	function __construct()
	{
		parent::__construct();
		
    }
    
    function admin(){
         
		$this->view->estilo=$this->getEstilo();
		$this->view->render('titulacion/admin');
    }

	function index(){
		
		$this->view->estilo=$this->getEstilo();	
		$this->view->render('titulacion/index');
	}	

	public function guardar(){
        if(isset($_POST['id'])){
          $this->model->id = $_POST['id'];
        }
		    $this->model->nombre = $_POST['nombre'];
        $this->model->versioncap = $_POST['versioncap'];
        $this->model->edicion = $_POST['edicion'];
        $this->model->fechainicio = $_POST['fechainicio'];
        $this->model->fechafin = $_POST['fechafin'];
        $this->model->costo = $_POST['costo'];
        $this->model->porcentaje = $_POST['porcentaje'];
        $this->model->total = $_POST['total'];
        $this->model->estado = $_POST['estado'];
        $this->model->idtipo = $_POST['idtipo'];
        $this->model->idaprovacion = $_POST['idaprovacion'];
        $this->model->codigo = $_POST['codigo'];
        
        if($this->model->id >0){
          echo $this->model->Actualizar($this->model);
        }else{
          echo $this->model->Registrar($this->model);
        }
	}

	public function listarJSON(){
		echo $this->model->ListarJSON();
	}	
  
  public function eliminar(){
		$id=$_POST['id'];		
		echo $this->model->Eliminar($id);
		
	}
    /***titullacion programa* */

    public function guardarPrograma(){
      $idtitulo=$_POST['idtitulo'];
      $idprograma=$_POST['idprograma'];
      echo $this->model->GuardarPrograma($idtitulo,$idprograma);
    }

    public function guardarRequisito(){
      $idtitulo=$_POST['idtitulo'];
      $idrequisito=$_POST['idrequisito'];
      echo $this->model->GuardarRequisito($idtitulo,$idrequisito);
    }

    public function listarPrograma(){
      $idtitulo=$_POST['idtitulo'];
      echo $this->model->ListarPrograma($idtitulo);
    }

    public function listarRequisito(){
      $idtitulo=$_POST['idtitulo'];
      echo $this->model->ListarRequisito($idtitulo);
    }
}
 ?>
