<?php

/**
 *
 */
class usuarioController extends Controlador
{

    function __construct()
    {
        parent::__construct();

    }

    function admin(){
        $this->view->estilo=$this->getEstilo();
        $this->view->render('usuario/admin');
    }


    function administrador(){
        $this->view->estilo=$this->getEstilo();
        $this->view->render('usuario/administrador');
    }

    function transcriptor(){
        $this->view->estilo=$this->getEstilo();
        $this->view->render('usuario/transcriptor');
    }

    function docente(){
        $this->view->estilo=$this->getEstilo();
        $this->view->render('usuario/docente');
    }

    function estudiante(){
        $this->view->estilo=$this->getEstilo();
        $this->view->render('usuario/estudiante');
    }

    function index(){

        $this->view->estilo=$this->getEstilo();
        $this->view->render('usuario/index');
    }

    public function GuardarAll(){
        if(isset($_POST['id'])){
            $this->model->id=$_POST['id'];
        }
        $this->model->nombre = $_POST['nombre'];
        $this->model->apellido = $_POST['apellido'];
        $this->model->nacionalidad = $_POST['nacionalidad'];
        $this->model->fechanacimiento = $_POST['fechanacimiento'];
        $this->model->lugarnacimiento = $_POST['lugarnacimiento'];
        $this->model->cedula = $_POST['cedula'];
        $this->model->calle = $_POST['calle'];
        $this->model->ciudad = $_POST['ciudad'];
        $this->model->telefono = $_POST['telefono'];
        $this->model->correo = $_POST['correo'];
        $this->model->contrasena = $_POST['contrasena'];
        $this->model->telefonotrabajo = $_POST['telefonotrabajo'];
        $this->model->correotrabajo = $_POST['correotrabajo'];
        $this->model->universidad = $_POST['universidad'];
        $this->model->licenciatura = $_POST['licenciatura'];
        $this->model->modalidad = $_POST['modalidad'];
        $this->model->otrosestudios = $_POST['otrosestudios'];
        $this->model->sexo = $_POST['sexo'];
        $this->model->tipo = $_POST['tipo'];

        if($this->model->id > 0){
            echo $this->model->Actualizar($this->model);
        }else{
            echo $this->model->Registrar($this->model);
        }

        //require_once 'views/usuario/index.php';
    }

    public function guardar(){
        if(isset($_POST['id'])){
            $this->model->id=$_POST['id'];
        }
        $this->model->nombre = $_POST['nombre'];
        $this->model->apellido = $_POST['apellido'];
        $this->model->nacionalidad = $_POST['nacionalidad'];
        $this->model->fechanacimiento = $_POST['fechanacimiento'];
        $this->model->lugarnacimiento = $_POST['lugarnacimiento'];
        $this->model->cedula = $_POST['cedula'];
        $this->model->calle = $_POST['calle'];
        $this->model->ciudad = $_POST['ciudad'];
        $this->model->telefono = $_POST['telefono'];
        $this->model->correo = $_POST['correo'];
        $this->model->contrasena = $_POST['contrasena'];
        $this->model->sexo = $_POST['sexo'];
        $this->model->tipo = $_POST['tipo'];

        if($this->model->id > 0){
            echo $this->model->ActualizarUsuario($this->model);
        }else{
            echo $this->model->RegistrarUsuario($this->model);
        }
    }

    public function eliminar(){
        $id=$_POST['id'];
        echo $this->model->Eliminar($id);
    }

    public function listarUsuarioJSON(){
        $tipo=$_GET['tipo'];
        echo $this->model->ListarUsuarioJSON($tipo);
    }

}