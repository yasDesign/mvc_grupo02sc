<?php

/**
* 
*/
class Controlador
{
	public $model;
	public $view;
	function __construct()
	{
		$this->view=new Vista();
	}

	public function initModel($prt_model){
		$path="models/".$prt_model.".php";
		if (file_exists($path)) {
			require $path;
			$modelName=$prt_model."Model";
			$this->model=new $modelName;
		}
	}

	public function visitar(){
		$id=empty($_GET['id'])? 1:$_GET['id'];
		$res=$this->model->visitar($id);
		if($id==1){
			return $res;
		}else{
			echo $res;
		}
	}

	public function getEstilo(){
		return $this->model->getEstilo();
		//return "default.css";
	}

	public function estilos(){
		echo $this->model->Estilos();
	
	}

	public function activarEstilo(){
		$id=$_POST['id'];
		$this->model->ActivarEstilo($id);
		echo "ok";
		//header('location:../admin');
    }

}


?>