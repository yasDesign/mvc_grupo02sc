<?php

/**
* 
*/
include_once('libs/db.php');
class Modelo
{
	public $pdo;
	function __construct(){	
		
		try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {                                                           
            die($e->getMessage());
        }	
	}

	public function visitar($id){
		try
        {
            $stm = $this->pdo->prepare("SELECT * FROM visitas where id= ?");
            $stm->execute(array($id));
			$res=$stm->fetch(PDO::FETCH_OBJ);
			$res=$res->contador+1;
			
			$sql = "UPDATE visitas SET contador =? WHERE id	= ?";
			$this->pdo->prepare($sql)->execute(array($res,$id));
			if($id==1){
				return $res;
			}else{
				return json_encode(array('contador'=>$res));
			}		
			
        } catch (Exception $e)
        {
            die($e->getMessage());
        }		
	}

	/*public function getEstilo(){
		$sql="SELECT * FROM estilos WHERE estado=1;";
		$this->db->setFetchMode(ADODB_FETCH_ASSOC);
		$result=$this->db->Execute($sql);
		//print_r($result);
		return $result->fetchRow()['archivo'];
		//return 'default.css';

	}*/
	
	public function getEstilo(){
		try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM estilo where estado=? LIMIT 1");
            $stm->execute(array(true));

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
	}

	public function Estilos(){
		try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM estilo");
            $stm->execute(array());
            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }
        
    public function ActivarEstilo($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("UPDATE estilo set estado=?");
            $stm->execute(array(0));

            $stm = $this->pdo
                ->prepare("UPDATE estilo set estado=? where id=?");
            $stm->execute(array(1,$id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}


?>