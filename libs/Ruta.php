<?php 


class Ruta
{
	
	function __construct()
	{
		//echo "Asdas";
		$url=isset($_GET['url'])? $_GET['url']:null;
		if (empty($url)) {
			require "controllers/inicio.controller.php";
			$controller=new inicioController();
			$controller->initModel('inicio');
			$controller->index();

		}else{
			$url=rtrim($url,'/');
			$url=explode("/",$url);
			$file="controllers/".$url[0].".controller.php";

			if (file_exists($file)) {
				require $file;
				$modellocal=$url[0];
				$url[0]=$url[0].'Controller';
				$controller=new $url[0];
				$controller->initModel($modellocal);

				if (isset($url[2])) {
						if (method_exists($controller,$url[1])) {
							$controller->{$url[1]}($url[2]);		
						}else{
							require "controllers/falla.php";
							$controller=new Falla();
							$controller->initModel('falla');
							$controller->index();
						}
				}else{
					if (isset($url[1])) {
						if (method_exists($controller,$url[1])) {
							$controller->{$url[1]}();
						}else{
							require "controllers/falla.php";
							$controller=new Falla();
							$controller->initModel('falla');
							$controller->index();
						}
					}else{
						$controller->index();
					}
				}				
				
			}else{
				require "controllers/falla.php";
				$controller=new Falla();
				$controller->initModel('falla');
				$controller->index();
			}
		}
	}
}

 ?>