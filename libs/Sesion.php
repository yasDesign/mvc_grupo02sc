<?php 	

/**
* 
*/
class Sesion
{
	
	function __construct()
	{
	}

	public static function init(){
		@session_start();
	}

	public static function set($prt_key,$prt_value){
		$_SESSION[$prt_key]=$prt_value;
	}

	public static function get($prt_key){
		if (isset($_SESSION[$prt_key])) {
			return $_SESSION[$prt_key];
		}
	}

	public static function destroy(){
		session_destroy();
	}

	public static function cerrar(){
		session_write_close();
	}

}

 ?>