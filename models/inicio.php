<?php


class inicioModel extends Modelo
{
    
    public $id;
    public $nombre;
    public $apellido;
    public $nacionalidad;
    public $fechanacimiento;
    public $lugarnacimiento;

    public $cedula;
    public $calle;
    public $ciudad;
    public $telefono;
    public $correo;

    public $contrasena;
    public $calletrabajo;
    public $ciudadtrabajo;
    public $telefonotrabajo;
    public $correotrabajo;
    public $licenciatura;

    public $universidad;
    public $modalidad;
    public $otrosestudios;


    function __construct()
	{
        parent::__construct();
        
	}

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM inicio where tipo=3");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }                                                                                                                                                                                                                                                                                                                                                                               

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM inicio WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Cantidad($correo,$contrasena)
    {
        try
        {
            $stm = $this->pdo->prepare("SELECT count(*) as cantidad FROM usuario WHERE correo = ? and contrasena= ? ");
            $stm->execute(array($correo,$contrasena));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function getUsuario($correo,$contrasena)
    {
        try
        {
            $stm = $this->pdo->prepare("SELECT * FROM usuario WHERE correo = ? and contrasena= ? LIMIT 1");
            $stm->execute(array($correo,$contrasena));
            return $stm->fetch();
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM inicio WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE inicio SET 
						nombre          = ?,
						apellido	    = ?,
				        nacionalidad    =?,
                        fechanacimiento =?,
                        lugarnacimiento =?,
                        cedula          =?,
                        calle           =?,
                        ciudad          =?,
                        telefono        =?,
                        correo          =?,
                        contrasena      =?,
                        calletrabajo    =?,
                        ciudadtrabajo   =?,
                        telefonotrabajo =?,
                        correotrabajo   =?,
                        licenciatura    =?,
                        universidad     =?,
                        modalidad       =?,
                        otrosestudios   =?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,

                        $data->contrasena,
                        $data->calletrabajo,
                        $data->ciudadtrabajo,
                        $data->telefonotrabajo,
                        $data->correotrabajo,
                        $data->licenciatura,

                        $data->universidad,
                        $data->modalidad,
                        $data->otrosestudios,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(inicioModel $data)
    {
        try
        {
            $sql = "INSERT INTO inicio (nombre, apellido,nacionalidad,fechanacimiento,lugarnacimiento,cedula,calle,ciudad,telefono,correo,contrasena,calletrabajo,ciudadtrabajo,telefonotrabajo,correotrabajo,licenciatura,universidad,modalidad,otrosestudios) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,

                        $data->contrasena,
                        $data->calletrabajo,
                        $data->ciudadtrabajo,
                        $data->telefonotrabajo,
                        $data->correotrabajo,
                        $data->licenciatura,

                        $data->universidad,
                        $data->modalidad,
                        $data->otrosestudios
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function RegistrarEstudiante(inicioModel $data)
    {
        try
        {
            $sql = "INSERT INTO usuario (nombre, apellido,nacionalidad,fechanacimiento,lugarnacimiento,cedula,calle,ciudad,telefono,correo,contrasena) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,
                        $data->contrasena,
            
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
