<?php


class inscripcionModel extends Modelo
{
    
    public $id;
    public $fecha;
    public $detalle;
    public $codigo;
    public $idusuario;

    function __construct()
	{
        parent::__construct();
        
	}

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM inscripcion");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function ListarJSON()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM inscripcion");
            $stm->execute();

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    } 

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM inscripcion WHERE id = ? ORDER BY id");


            $stm->execute(array($id));
            var_dump($stm);
            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function obtenerInsc($id)
    {
        try
        {
            $stm = $this->pdo->prepare("select * 
from inscripcion, detalleinscripcion, titulacion
where inscripcion.id = detalleinscripcion.idinscripcion
  and detalleinscripcion.idtitulacion = titulacion.id 
  and inscripcion.idestudiante id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM inscripcion WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE inscripcion SET 
						fecha          = ?,
						detalle	    = ?,
						idusuario   = ?,
						idtranscriptor  =?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->fecha,
                        $data->detalle,
                        $data->idusuario,
                        $data->idtranscriptor,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(inscripcionModel $data)
    {
        try
        {
            $sql = "INSERT INTO inscripcion (fecha, detalle, idusuario, idtranscriptor) VALUES (?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->fecha,
                        $data->detalle,
                        $data->idusuario,
                        $data->idtranscriptor,
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

}
