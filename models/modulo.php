<?php


class moduloModel extends Modelo
{
    
    public $id;
    public $nombre;
    public $descripcion;
    public $docente;
    public $estado;
    public $combalidacion;


    function __construct()
	{
        parent::__construct();
        
	}

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM modulo");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }                                                                                                                                                                                                                                                                                                                                                                               

    public function ListarJSON()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM modulo");
            $stm->execute();

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    } 

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM modulo WHERE id = ? ORDER BY id");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM modulo WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE modulo SET 
						nombre          = ?,
						descripcion	    = ?,
						docente         = ?,
						estado          = ?,
						combalidacion   = ?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                        $data->docente,
                        $data->estado,
                        $data->combalidacion,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(moduloModel $data)
    {
        try
        {
            $sql = "INSERT INTO modulo (nombre, descripcion, docente, estado, combalidacion) VALUES (?,?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                        $data->docente,
                        $data->estado,
                        $data->combalidacion,
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

}
