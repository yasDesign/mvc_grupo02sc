<?php


class requisitoModel extends Modelo
{
    
    public $id;
    public $nombre;
    public $descripcion;

    function __construct()
	{
        parent::__construct();
        
	}

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM requisito");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }                                                                                                                                                                                                                                                                                                                                                                               

    public function ListarJSON()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM requisito");
            $stm->execute();

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    } 

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM requisito WHERE id = ? ORDER BY id");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM requisito WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE requisito SET 
						nombre          = ?,
						descripcion	    = ?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(requisitoModel $data)
    {
        try
        {
            $sql = "INSERT INTO requisito (nombre, descripcion) VALUES (?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

}