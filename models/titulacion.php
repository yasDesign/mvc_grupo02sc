<?php


class titulacionModel extends Modelo
{
    
    public $id;
    public $nombre;
    public $versioncap;
    public $edicion;
    public $created;
    public $fechainicio;
    public $fechafin;
    public $costo;
    public $porcentaje;
    public $total;
    public $estado;
    public $idtipo;
    public $idprovacion;
    public $codigo;

    function __construct()
	{
        parent::__construct();
        
	}

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM titulacion");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }                                                                                                                                                                                                                                                                                                                                                                               

    public function ListarJSON()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM titulacion");
            $stm->execute();

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    } 

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM titulacion WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM titulacion WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar(titulacionModel $data)
    {
        try
        {
            $sql = "UPDATE titulacion SET 
						nombre          = ?,
						versioncap	    = ?,
						edicion     = ?,
						fechainicio   =?,
						fechafin      =?,
						costo     = ?,
						porcentaje = ?,
						total   = ?,
						estado  = ?,
						idtipo = ?,
						idaprovacion = ?,
						codigo = ?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->versioncap,
                        $data->edicion,
                        $data->fechainicio,
                        $data->fechafin,
                        $data->costo,
                        $data->porcentaje,
                        $data->total,
                        $data->estado,
                        $data->idtipo,
                        $data->idaprovacion,
                        $data->codigo,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(titulacionModel $data)
    {
        try
        {
            $sql = "INSERT INTO titulacion ( nombre, versioncap, edicion, fechainicio, fechafin, costo, porcentaje, total, estado, idtipo, idaprovacion, codigo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->versioncap,
                        $data->edicion,
                        $data->fechainicio,
                        $data->fechafin,
                        $data->costo,
                        $data->porcentaje,
                        $data->total,
                        $data->estado,
                        $data->idtipo,
                        $data->idaprovacion,
                        $data->codigo
                        )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function GuardarPrograma($idtitulo,$idprograma)
    {
        try
        {
            $sql = "INSERT INTO titulacionprograma ( idtitulo, idprograma) VALUES (?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $idtitulo,
                        $idprograma
                        )
                );
                return 1;
        } catch (Exception $e)
        {
            return 0;
            //die($e->getMessage());
        }
    }

    public function GuardarRequisito($idtitulo,$idrequisito)
    {
        try
        {
            $sql = "INSERT INTO titulacionrequisito ( idtitulo, idrequisito) VALUES (?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $idtitulo,
                        $idrequisito
                        )
                );
                return 1;
        } catch (Exception $e)
        {
            return 0;
            //die($e->getMessage());
        }
    }

    public function ListarPrograma($idtitulo){
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT idprograma FROM titulacionprograma WHERE idtitulo=?");
            $stm->execute(array($idtitulo));

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }
    

    

    public function ListarRequisito($idtitulo){
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT idrequisito FROM titulacionrequisito WHERE idtitulo=?");
            $stm->execute(array($idtitulo));

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }
}
