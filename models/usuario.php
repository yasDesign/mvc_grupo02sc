<?php


class usuarioModel extends Modelo
{


    public $id;
    public $nombre;
    public $apellido;
    public $nacionalidad;
    public $fechanacimiento;
    public $lugarnacimiento;

    public $cedula;
    public $calle;
    public $ciudad;
    public $telefono;
    public $correo;

    public $contrasena;
    public $calletrabajo;
    public $ciudadtrabajo;
    public $telefonotrabajo;
    public $correotrabajo;
    public $licenciatura;

    public $universidad;
    public $modalidad;
    public $otrosestudios;

    public $sexo;
    public $codigo;


    public function __CONSTRUCT()
    {
        parent::__CONSTRUCT();
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM usuario");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function ListarJSON(){
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM usuario");
            $stm->execute();

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function ListarUsuarioJSON($tipo){
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM usuario where tipo=?");
            $stm->execute(array($tipo));

            return json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM usuario WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM usuario WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE usuario SET 
						nombre          = ?,
						apellido	    = ?,
				        nacionalidad    =?,
                        fechanacimiento =?,
                        lugarnacimiento =?,
                        cedula          =?,
                        calle           =?,
                        ciudad          =?,
                        telefono        =?,
                        correo          =?,
                        contrasena      =?,
                        calletrabajo    =?,
                        telefonotrabajo =?,
                        correotrabajo   =?,
                        licenciatura    =?,
                        universidad     =?,
                        modalidad       =?,
                        otrosestudios   =?,
                        sexo            =?,
                        tipo            =?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,

                        $data->contrasena,
                        $data->calletrabajo,
                        $data->telefonotrabajo,
                        $data->correotrabajo,
                        $data->licenciatura,

                        $data->universidad,
                        $data->modalidad,
                        $data->otrosestudios,
                        $data->sexo,
                        $data->tipo,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(usuarioModel $data)
    {
        try
        {
            $sql = "INSERT INTO usuario (nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, cedula, calle, ciudad, telefono, correo, contrasena,  telefonotrabajo, correotrabajo, licenciatura, universidad, modalidad, otrosestudios, sexo, tipo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,

                        $data->contrasena,
                        $data->telefonotrabajo,
                        $data->correotrabajo,
                        $data->licenciatura,

                        $data->universidad,
                        $data->modalidad,
                        $data->otrosestudios,
                        $data->sexo,
                        $data->tipo
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function RegistrarEstudiante(usuarioModel $data)
    {
        try
        {
            $sql = "INSERT INTO usuario (nombre, apellido,nacionalidad,fechanacimiento,lugarnacimiento,cedula,calle,ciudad,telefono,correo,contrasena) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,
                        $data->contrasena,

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function RegistrarUsuario($data){

        try
        {
            $sql = "INSERT INTO usuario (nombre, apellido,nacionalidad,fechanacimiento,lugarnacimiento,cedula,calle,ciudad,telefono,correo,contrasena,sexo,tipo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,

                        $data->contrasena,
                        $data->sexo,
                        $data->tipo

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function ActualizarUsuario($data)
    {
        try
        {
            $sql = "UPDATE usuario SET 
						nombre          = ?,
						apellido	    = ?,
				        nacionalidad    =?,
                        fechanacimiento =?,
                        lugarnacimiento =?,
                        cedula          =?,
                        calle           =?,
                        ciudad          =?,
                        telefono        =?,
                        correo          =?,
                        contrasena      =?,
                        sexo      =?,
                        tipo   =?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,
                        $data->contrasena,
                        $data->sexo,
                        $data->tipo,                        
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

}
