
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",[]);

app.controller("adminAprobacionController",function($scope,$http){

    $scope.l_aprobacions=[];
    listarAprobacion();

    function listarAprobacion(){
        $http.get(url_root+"aprobacion/listarJSON").then(function(response){
            $scope.l_aprobacions=response.data;
            console.log(response.data);
        });
    }

    $scope.addSubmit=function(){
        console.log("ver");
        console.log($scope.newAprobacion);
        var fd=new FormData();
        fd.append("fecha",$scope.newAprobacion.fecha);
        fd.append("autoridad",$scope.newAprobacion.autoridad);

        $http.post(url_root+"aprobacion/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarAprobacion();
        });
        $('#modalAdd').modal('hide');
    }

    $scope.selectItem=function($i_aprobacion){
        $scope.selectedAprobacion=$i_aprobacion;
    }

    $scope.reset=function(){
        $scope.newAprobacion={};
    }
});
