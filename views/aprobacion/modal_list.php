<div class="form-group">
    <input type="text" class="form-control">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title">Id </th>
            <th class="column-title">Nombre </th>
            <th class="column-title">Descripcion </th>
            <th class="column-title">Accion </th>
        </tr>
        </thead>

        <tbody>
        <tr class="even pointer" ng-repeat="i_aprobacion in l_aprobacions">
            <td class=" ">{{i_aprobacion.id}}</td>
            <td class=" ">{{i_aprobacion.fecha}}</td>
            <td class=" ">{{i_aprobacion.autoridad}} </td>
            <td>
                <button class="btn btn-primary" type="reset" ng-click="selectItem(i_aprobacion)" data-target="#modalEdit" data-toggle="modal">Editar</button>
                <button class="btn btn-danger" type="reset" ng-click="selectItem(i_aprobacion)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>