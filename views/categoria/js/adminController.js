
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",[]);

app.controller("adminCategoriaController",function($scope,$http){
    $scope.newCategoria={};

    $scope.l_categorias=[];
    $scope.selectedCategoria={};
    listarCategorias();

    function listarCategorias(){
        $http.get(url_root+"categoria/listarJSON").then(function(response){
            $scope.l_categorias=response.data;
            prepararReporte();
            console.log(response.data);
        });
    }

    $scope.addSubmit=function(){
        console.log($scope.newCategoria);
        var fd=new FormData();
        fd.append("nombre",$scope.newCategoria.nombre);
        fd.append("descripcion",$scope.newCategoria.descripcion);
        
        $http.post(url_root+"categoria/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
            }).then(function(response){
                console.log(response.data);
                listarCategorias();
                $('#modalAdd').modal('hide');
                $scope.reset();
            });
    }

    $scope.editSubmit=function(){
        
        console.log($scope.selectedCategoria);
        var fd=new FormData();
        fd.append("id",$scope.selectedCategoria.id);
        fd.append("nombre",$scope.selectedCategoria.nombre);
        fd.append("descripcion",$scope.selectedCategoria.descripcion);
        
        $http.post(url_root+"categoria/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
                console.log(response.data);
                listarCategorias();
                $('#modalEdit').modal('hide');
                $scope.reset();
        });
    }

    $scope.deleteSubmit=function(){
        
        console.log($scope.selectedCategoria);
        var fd=new FormData();
        fd.append("id",$scope.selectedCategoria.id);
        
        $http.post(url_root+"categoria/eliminar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
                console.log(response.data);
                listarCategorias();
                $('#modalDelete').modal('hide');
                $scope.reset();
        });
    }

    $scope.selectItem=function($i_categoria){
        $scope.selectedCategoria.id=$i_categoria.id;
        $scope.selectedCategoria.nombre=$i_categoria.nombre;
        $scope.selectedCategoria.descripcion=$i_categoria.descripcion;

    }

    $scope.reset=function(){
        $scope.newCategoria={};
        $scope.selectedCategoria={};
    }


    function prepararReporte() {
        $scope.fileName = "reportecCategoria";
        $scope.exportData = [];
        $scope.exportData.push(["id", "nombre", "descripcion"]);
        angular.forEach($scope.l_categorias, function (m) {
            $scope.exportData.push([m.id, m.nombre, m.descripcion]);
        });
    }
});


app.directive('excelExport',
    function () {
        return {
            restrict: 'A',
            scope: {
                fileName: "@",
                data: "&exportData"
            },
            replace: true,
            template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()">Export to Excel <i class="fa fa-download"></i></button>',
            link: function (scope, element) {

                scope.download = function () {

                    function datenum(v, date1904) {
                        if (date1904) v += 1462;
                        var epoch = Date.parse(v);
                        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                    };

                    function getSheet(data, opts) {
                        var ws = {};
                        var range = {
                            s: {
                                c: 10000000,
                                r: 10000000
                            },
                            e: {
                                c: 0,
                                r: 0
                            }
                        };
                        for (var R = 0; R != data.length; ++R) {
                            for (var C = 0; C != data[R].length; ++C) {
                                if (range.s.r > R) range.s.r = R;
                                if (range.s.c > C) range.s.c = C;
                                if (range.e.r < R) range.e.r = R;
                                if (range.e.c < C) range.e.c = C;
                                var cell = {
                                    v: data[R][C]
                                };
                                if (cell.v == null) continue;
                                var cell_ref = XLSX.utils.encode_cell({
                                    c: C,
                                    r: R
                                });

                                if (typeof cell.v === 'number') cell.t = 'n';
                                else if (typeof cell.v === 'boolean') cell.t = 'b';
                                else if (cell.v instanceof Date) {
                                    cell.t = 'n';
                                    cell.z = XLSX.SSF._table[14];
                                    cell.v = datenum(cell.v);
                                } else cell.t = 's';

                                ws[cell_ref] = cell;
                            }
                        }
                        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                        return ws;
                    };

                    function Workbook() {
                        if (!(this instanceof Workbook)) return new Workbook();
                        this.SheetNames = [];
                        this.Sheets = {};
                    }

                    var wb = new Workbook(),
                        ws = getSheet(scope.data());
                    /* add worksheet to workbook */
                    wb.SheetNames.push(scope.fileName);
                    wb.Sheets[scope.fileName] = ws;
                    var wbout = XLSX.write(wb, {
                        bookType: 'xlsx',
                        bookSST: true,
                        type: 'binary'
                    });

                    function s2ab(s) {
                        var buf = new ArrayBuffer(s.length);
                        var view = new Uint8Array(buf);
                        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                        return buf;
                    }

                    saveAs(new Blob([s2ab(wbout)], {
                        type: "application/octet-stream"
                    }), scope.fileName + '.xlsx');

                };

            }
        };
    }
);
