
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",[]);

app.controller("adminDashboardController",function($scope,$http,$window){
    $scope.newEstilo={};

    $scope.l_estilos=[];
    $scope.selectedEstilo={};

    listarEstilos();

    function listarEstilos(){
        $http.get(url_root+"inicio/estilos").then(function(response){
            $scope.l_estilos=response.data;
            console.log(response.data);
            
        });
    }

    $scope.activeSubmit=function(){
        console.log($scope.selectedEstilo);
        if($scope.selectedEstilo.id>0){ 

            console.log($scope.selectedEstilo);
            var fd=new FormData();
            fd.append("id",$scope.selectedEstilo.id);
            fd.append("descripcion",$scope.newEstilo.descripcion);
            
            $http.post(url_root+"inicio/activarEstilo",fd,{
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined,
                    'Process-Data': false
                }
            }).then(function(response){
                console.log(response.data);
                $window.location.href = url_root+"inicio/dashboard";
            });

        }else{
            console.log("no selected");
        }
        
    }
   
   
});
