<div class="form-group">
    <input type="text" class="form-control" ng-model="searchCategoria">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title">Id </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">Descripcion </th>
                <th class="column-title">Accion </th>
            </tr>
        </thead>

        <tbody>
            <tr class="even pointer" ng-repeat="i_categoria in l_categorias | filter:searchCategoria |orderBy:'id'">
                <td class=" ">{{i_categoria.id}}</td>
                <td class=" ">{{i_categoria.nombre}}</td>
                <td class=" ">{{i_categoria.descripcion}} </td>
                <td>
                    <button class="btn btn-primary" type="reset" ng-click="selectItem(i_categoria)" data-target="#modalEdit" data-toggle="modal">Editar</button> 
                    <button class="btn btn-danger" type="reset" ng-click="selectItem(i_categoria)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>