
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",[]);

app.controller("adminEstudianteController",function($scope,$http){

    $scope.l_estudiantes=[];
    $scope.selectedEstudiantes=[];
    listarEstudiantes();

    function listarEstudiantes(){
        $http.get(url_root+"estudiante/listarJSON").then(function(response){
            $scope.l_estudiantes=response.data;
            console.log(response.data);
        });
    }

    $scope.addSubmit=function(){
        console.log("ver");
        console.log($scope.newEstudiantes);
        var fd=new FormData();
        fd.append("nombre",$scope.newEstudiantes.nombre);
        fd.append("apellido",$scope.newEstudiantes.apellido);
        fd.append("nacionalidad",$scope.newEstudiantes.nacionalidad);
        fd.append("fechanacimiento",$scope.newEstudiantes.fechanacimiento);
        fd.append("lugarnacimiento",$scope.newEstudiantes.lugarnacimiento);
        fd.append("cedula",$scope.newEstudiantes.cedula);
        fd.append("calle",$scope.newEstudiantes.calle);
        fd.append("ciudad",$scope.newEstudiantes.ciudad);
        fd.append("telefono",$scope.newEstudiantes.telefono);
        fd.append("correo",$scope.newEstudiantes.correo);
        fd.append("contrasena",$scope.newEstudiantes.contrasena);
        fd.append("sexo",$scope.newEstudiantes.sexo);
        fd.append("tipo",2);


        $http.post(url_root+"estudiante/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarEstudiantes();
            $('#modalAdd').modal('hide');
            $scope.reset();
        });
    }

    $scope.editSubmit=function(){

        console.log($scope.selectedEstudiantes);
        var fd=new FormData();
        fd.append("id",$scope.selectedEstudiantes.id);
        fd.append("nombre",$scope.selectedEstudiantes.apellido);
        fd.append("apellido",$scope.selectedEstudiantes.apellido);
        fd.append("nacionalidad",$scope.selectedEstudiantes.nacionalidad);
        fd.append("fechanacimiento",$scope.selectedEstudiantes.fechanacimiento);
        fd.append("lugarnacimiento",$scope.selectedEstudiantes.lugarnacimiento);
        fd.append("cedula",$scope.selectedEstudiantes.cedula);
        fd.append("calle",$scope.selectedEstudiantes.calle);
        fd.append("ciudad",$scope.selectedEstudiantes.ciudad);
        fd.append("telefono",$scope.selectedEstudiantes.telefono);
        fd.append("correo",$scope.selectedEstudiantes.correo);
        fd.append("contrasena",$scope.selectedEstudiantes.contrasena);
        fd.append("sexo",$scope.selectedEstudiantes.sexo);
        fd.append("tipo",$scope.selectedEstudiantes.tipo);

        $http.post(url_root+"estudiante/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarEstudiantes();
            $('#modalEdit').modal('hide');
            $scope.reset();
        });
    }

    $scope.deleteSubmit=function(){

        console.log($scope.selectedEstudiantes);
        var fd=new FormData();
        fd.append("id",$scope.selectedEstudiantes.id);

        $http.post(url_root+"estudiante/eliminar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarEstudiantes();
            $('#modalDelete').modal('hide');
            $scope.reset();
        });
    }


    $scope.selectItem=function($i_estudiante){
        $scope.selectedEstudiantes.id=$i_estudiante.id;
        $scope.selectedEstudiantes.nombre=$i_estudiante.nombre;
        $scope.selectedEstudiantes.apellido=$i_estudiante.apellido;
        $scope.selectedEstudiantes.nacionalidad=$i_estudiante.nacionalidad;
        $scope.selectedEstudiantes.fechanacimiento=$i_estudiante.fechanacimiento;
        $scope.selectedEstudiantes.lugarnacimiento=$i_estudiante.lugarnacimiento;
        $scope.selectedEstudiantes.cedula=$i_estudiante.cedula;
        $scope.selectedEstudiantes.calle=$i_estudiante.calle;
        $scope.selectedEstudiantes.ciudad=$i_estudiante.ciudad;
        $scope.selectedEstudiantes.telefono=$i_estudiante.telefono;
        $scope.selectedEstudiantes.correo=$i_estudiante.correo;
        $scope.selectedEstudiantes.contrasena=$i_estudiante.contrasena;
        $scope.selectedEstudiantes.sexo=$i_estudiante.sexo;
        $scope.selectedEstudiantes.tipo=$i_estudiante.tipo;

    }

    $scope.reset=function(){
        $scope.newEstudiantes={};
        $scope.selectedEstudiantes={};
    }
});
