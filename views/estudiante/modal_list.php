<div class="form-group">
    <input type="text" class="form-control">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title">Id </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">apellido </th>
                <th class="column-title">nacionalidad </th>
                <th class="column-title">telefono </th>
                <th class="column-title">tipo </th>
                <th class="column-title">Accion </th>

            </tr>
        </thead>

        <tbody>
            <tr class="even pointer" ng-repeat="$i_estudiante in l_estudiantes">
                <td class=" ">{{$i_estudiante.id}}</td>
                <td class=" ">{{$i_estudiante.nombre}}</td>
                <td class=" ">{{$i_estudiante.apellido}}</td>
                <td class=" ">{{$i_estudiante.nacionalidad}} </td>
                <td class=" ">{{$i_estudiante.telefono}} </td>
                <td class=" ">{{$i_estudiante.tipo}} </td>

                <td>
                    <button class="btn btn-primary" type="reset" ng-click="selectItem($i_estudiante)" data-target="#modalEdit" data-toggle="modal">Editar</button>
                    <button class="btn btn-danger" type="reset" ng-click="selectItem($i_estudiante)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>