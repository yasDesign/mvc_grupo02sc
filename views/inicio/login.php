<body class="login">
  <div ng-controller="loginController">
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">
          <form ng-submit="submitLogin()">
            <h1>{{modelo}}</h1>
            <div>
              <input type="text" class="form-control" placeholder="Username" required="" ng-model="login.correo"/>
            </div>
            <div>
              <input type="password" class="form-control" placeholder="Password" required="" ng-model="login.contrasena"/>
            </div>
            <div>
              <button class="btn btn-default submit" type="submit">Iniciar</button>
              <a class="reset_pass" href="#">Lost your password?</a>
            </div>

            <div class="clearfix"></div>

            <div class="separator">
              <p class="change_link">New to site?
                <a href="#signup" class="to_register"> Create Account </a>
              </p>

              <div class="clearfix"></div>
              <br />


            </div>
          </form>
        </section>
      </div>

      <div id="register" class="animate form registration_form">
        <section class="login_content">
        </section>
      </div>
    </div>
  </div>
  <script src="<?php echo URL_ROOT."views/inicio/js/loginController.js ";?>"></script>