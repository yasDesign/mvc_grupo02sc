
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-center">Registro de Usuarios</h2>
                </div>
                <div class="card-body">
                    <form action="<?php echo URL_ROOT;?>inicio/guardar" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" name="nombre" class="form-control" required>
                                </div>            
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="apellido">Apellido</label>
                                    <input type="text" name="apellido" class="form-control" required>
                                </div>    
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nacionalidad">Nacionalidad</label>
                                    <input type="text" name="nacionalidad" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fechanacimiento">Fecha Nacimiento</label>
                                    <input type="date" name="fechanacimiento" class="form-control" required>
                                </div>        
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="cedula">Cedula</label>
                                    <input type="number" name="cedula" class="form-control" required min="0">
                                </div>    
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lugarnacimiento">Lugar Nacimiento</label>
                                    <input type="text" name="lugarnacimiento" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="calle">Calle</label>
                                    <input type="text" name="calle" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ciudad">Ciudad</label>
                                    <input type="text" name="ciudad" class="form-control" required>
                                </div>                            
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Telefono</label>
                                    <input type="number" name="telefono" class="form-control" required min="0">
                                </div>    
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="correo">Correo</label>
                                    <input type="email" name="correo" class="form-control" required>
                                </div>                            
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contrasena">Contrasena</label>
                                    <input type="password" name="contrasena" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary form-control">Iniciar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>    
        <div class="col-md-2"></div>
    </div>
</div>