
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",[]);

app.controller("adminInscripcionController",function($scope,$http){
    $scope.newInscripcion={};

    $scope.l_inscripcions=[];
    $scope.selectedInscripcion={};
    listarInscripcion();

    function listarInscripcion(){
        $http.get(url_root+"inscripcion/listarJSON").then(function(response){
            $scope.l_inscripcions=response.data;
            console.log(response.data);
        });
    }

    $scope.addSubmit=function(){
        console.log($scope.newInscripcion);
        var fd=new FormData();
        fd.append("fecha",$scope.newInscripcion.fecha);
        fd.append("detalle",$scope.newInscripcion.detalle);
        fd.append("idusuario",$scope.newInscripcion.idusuario);
        fd.append("idtranscriptor",$scope.newInscripcion.idtranscriptor);

        $http.post(url_root+"inscripcion/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarInscripcion();
            $('#modalAdd').modal('hide');
            $scope.reset();
        });
    }

    $scope.editSubmit=function(){

        console.log($scope.selectedInscripcion);
        var fd=new FormData();
        fd.append("id",$scope.selectedInscripcion.id);
        fd.append("fecha",$scope.selectedInscripcion.fecha);
        fd.append("detalle",$scope.selectedInscripcion.detalle);
        fd.append("idusuario",$scope.selectedInscripcion.idusuario);
        fd.append("idtranscriptor",$scope.selectedInscripcion.idtranscriptor);


        $http.post(url_root+"inscripcion/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarInscripcion();
            $('#modalEdit').modal('hide');
            $scope.reset();
        });
    }

    $scope.deleteSubmit=function(){

        console.log($scope.selectedInscripcion);
        var fd=new FormData();
        fd.append("id",$scope.selectedInscripcion.id);

        $http.post(url_root+"inscripcion/eliminar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarInscripcion();
            $('#modalDelete').modal('hide');
            $scope.reset();
        });
    }

    $scope.selectItem=function($i_inscripcion){
        $scope.selectedInscripcion.id=$i_inscripcion.id;
        $scope.selectedInscripcion.fecha=$i_inscripcion.fecha;
        $scope.selectedInscripcion.detalle=$i_inscripcion.detalle;
        $scope.selectedInscripcion.idusuario=$i_inscripcion.idusuario;
        $scope.selectedInscripcion.idtranscriptor=$i_inscripcion.idtranscriptor;


    }

    $scope.reset=function(){
        $scope.newInscripcion={};
        $scope.selectedInscripcion={};
    }
});
