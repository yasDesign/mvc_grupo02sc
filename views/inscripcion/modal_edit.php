<div class="modal" id="modalEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registro de {{ modelo }}</h4>
            </div>
            <div class="modal-body">
                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" ng-submit="editSubmit()">

                    <div class="form-group">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12 invisible" ng-model="selectedInscripcion.id">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha de la inscripcion
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="date" id="first-name" required="required" class="form-control col-md-7 col-xs-12" ng-model="selectedInscripcion.fecha">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Detalle de la inscripcion
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12" ng-model="selectedInscripcion.detalle">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Codigo del estudiante
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12" ng-model="selectedInscripcion.idusuario">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Codigo del estudiante
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="last-name" name="last-name" class="form-control col-md-7 col-xs-12" ng-model="selectedInscripcion.idtranscriptor">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    </form>
                    <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>                    
            </div>
        </div>
    </div>
</div>