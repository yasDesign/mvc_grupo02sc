<div class="form-group">
    <input type="text" class="form-control" ng-model="searchCategoria">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title">Id </th>
                <th class="column-title">fecha </th>
                <th class="column-title">detalle </th>
                <th class="column-title">codigo </th>
                <th class="column-title">usuario </th>
                <th class="column-title">transcriptor </th>

                <th class="column-title">Accion </th>
            </tr>
        </thead>

        <tbody>
            <tr class="even pointer" ng-repeat="i_inscripcion in l_inscripcions | filter:searchInscripcion |orderBy:'id'">
                <td class=" ">{{i_inscripcion.id}}</td>
                <td class=" ">{{i_inscripcion.fecha}}</td>
                <td class=" ">{{i_inscripcion.detalle}} </td>
                <td class=" ">{{i_inscripcion.codigo}} </td>
                <td class=" ">{{i_inscripcion.idusuario}} </td>
                <td class=" ">{{i_inscripcion.idtranscriptor}} </td>
                <td>
                    <button class="btn btn-primary" type="reset" ng-click="selectItem(i_inscripcion)" data-target="#modalEdit" data-toggle="modal">Editar</button>
                    <button class="btn btn-danger" type="reset" ng-click="selectItem(i_inscripcion)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>