//var url_root = "http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app = angular.module("app", []);

app.controller("adminModuloController", function ($scope, $http) {
    $scope.newModulo = {};

    $scope.l_modulos = [];
    $scope.selectedModulo = {};
    listarModulos();

    function listarModulos() {
        $http.get(url_root + "modulo/listarJSON").then(function (response) {
            $scope.l_modulos = response.data;
            console.log(response.data);
        });
    }

    $scope.addSubmit = function () {
        console.log($scope.newModulo);
        var fd = new FormData();
        fd.append("nombre", $scope.newModulo.nombre);
        fd.append("descripcion", $scope.newModulo.descripcion);
        fd.append("docente", $scope.newModulo.docente);
        fd.append("estado", $scope.newModulo.estado);
        fd.append("combalidacion", $scope.newModulo.combalidacion);

        $http.post(url_root + "modulo/guardar", fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function (response) {
            console.log(response.data);
            listarModulos();
            $('#modalAdd').modal('hide');
            $scope.reset();
        });
    }

    $scope.editSubmit = function () {

        console.log($scope.selectedModulo);
        var fd = new FormData();
        fd.append("id", $scope.selectedModulo.id);
        fd.append("nombre", $scope.selectedModulo.nombre);
        fd.append("descripcion", $scope.selectedModulo.descripcion);
        fd.append("docente", $scope.selectedModulo.docente);
        fd.append("estado", $scope.selectedModulo.estado);
        fd.append("combalidacion", $scope.selectedModulo.combalidacion);

        $http.post(url_root + "modulo/guardar", fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function (response) {
            console.log(response.data);
            listarModulos();
            $('#modalEdit').modal('hide');
            $scope.reset();
        });
    }

    $scope.deleteSubmit = function () {

        console.log($scope.selectedModulo);
        var fd = new FormData();
        fd.append("id", $scope.selectedModulo.id);

        $http.post(url_root + "modulo/eliminar", fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function (response) {
            console.log(response.data);
            listarModulos();
            $('#modalDelete').modal('hide');
            $scope.reset();
        });
    }

    $scope.selectItem = function ($i_Modulo) {
        $scope.selectedModulo.id = $i_Modulo.id;
        $scope.selectedModulo.nombre = $i_Modulo.nombre;
        $scope.selectedModulo.descripcion = $i_Modulo.descripcion;
        $scope.selectedModulo.docente = $i_Modulo.docente;
        $scope.selectedModulo.estado = $i_Modulo.estado;
        $scope.selectedModulo.combalidacion = $i_Modulo.combalidacion;

    }

    $scope.reset = function () {
        $scope.newModulo = {};
        $scope.selectedModulo = {};
    }
});
