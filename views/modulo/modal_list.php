<div class="form-group">
        <input type="text" class="form-control" ng-model="searchModulo">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title">Id </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">Descripcion </th>
                <th class="column-title">docente </th>
                <th class="column-title">estado </th>
                <th class="column-title">Descripcion </th>
                <th class="column-title">combalidacion </th>
                <th class="column-title">Accion </th>
            </tr>
        </thead>

        <tbody>
            <tr class="even pointer" ng-repeat="i_modulo in l_modulos | filter:searchModulo |orderBy:'id'">
                <td class=" ">{{i_modulo.id}}</td>
                <td class=" ">{{i_modulo.nombre}}</td>
                <td class=" ">{{i_modulo.descripcion}} </td>
                <td class=" ">{{i_modulo.docente}} </td>
                <td class=" ">{{i_modulo.estado}} </td>
                <td class=" ">{{i_modulo.combalidacion}} </td>

                <td>
                    <button class="btn btn-primary" type="reset" ng-click="selectItem(i_modulo)" data-target="#modalEdit" data-toggle="modal">Editar</button>
                    <button class="btn btn-danger" type="reset" ng-click="selectItem(i_modulo)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>