
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",[]);

app.controller("adminProgramaController",function($scope,$http){
    $scope.newPrograma={};

    $scope.l_programas=[];
    $scope.selectedPrograma={};
    listarPrograma();

    function listarPrograma(){
        $http.get(url_root+"programa/listarJSON").then(function(response){
            $scope.l_programas=response.data;
            console.log(response.data);
        });
    }

    $scope.addSubmit=function(){
        console.log("ver");
        console.log($scope.newPrograma);
        var fd=new FormData();
        fd.append("nombre",$scope.newPrograma.nombre);
        fd.append("descripcion",$scope.newPrograma.descripcion);
        
        $http.post(url_root+"programa/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
            }).then(function(response){
                console.log(response.data);
                listarPrograma();
            });
        $('#modalAdd').modal('hide');
    }

    $scope.editSubmit=function(){

        console.log($scope.selectedPrograma);
        var fd=new FormData();
        fd.append("id",$scope.selectedPrograma.id);
        fd.append("nombre",$scope.selectedPrograma.nombre);
        fd.append("descripcion",$scope.selectedPrograma.descripcion);

        $http.post(url_root+"programa/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarPrograma();
            $('#modalEdit').modal('hide');
            $scope.reset();
        });
    }

    $scope.deleteSubmit=function(){

        console.log($scope.selectedPrograma);
        var fd=new FormData();
        fd.append("id",$scope.selectedPrograma.id);

        $http.post(url_root+"programa/eliminar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarPrograma();
            $('#modalDelete').modal('hide');
            $scope.reset();
        });
    }

    $scope.selectItem=function($i_programa){
        $scope.selectedPrograma.id=$i_programa.id;
        $scope.selectedPrograma.nombre=$i_programa.nombre;
        $scope.selectedPrograma.descripcion=$i_programa.descripcion;

    }

    $scope.reset=function(){
        $scope.newPrograma={};
        $scope.selectedPrograma={};
    }
});
