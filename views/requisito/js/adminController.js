//var url_root = "http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app = angular.module("app", []);

app.controller("adminRequisitoController", function ($scope, $http) {
    $scope.newRequisito = {};
    $scope.modelo = "Requisito";

    $scope.l_requisitos = [];
    $scope.selectedRequisito = {};
    listarRequisitos();

    function listarRequisitos() {
        $http.get(url_root + "requisito/listarJSON").then(function (response) {
            $scope.l_requisitos = response.data;
            console.log(response.data);
            prepararReporte();
        });
    }

    $scope.addSubmit = function () {
        console.log($scope.newRequisito);
        var fd = new FormData();
        fd.append("nombre", $scope.newRequisito.nombre);
        fd.append("descripcion", $scope.newRequisito.descripcion);

        $http.post(url_root + "requisito/guardar", fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function (response) {
            console.log(response.data);
            listarRequisitos();
            $('#modalAdd').modal('hide');
            $scope.reset();
        });
    }

    $scope.editSubmit = function () {

        console.log($scope.selectedRequisito);
        var fd = new FormData();
        fd.append("id", $scope.selectedRequisito.id);
        fd.append("nombre", $scope.selectedRequisito.nombre);
        fd.append("descripcion", $scope.selectedRequisito.descripcion);

        $http.post(url_root + "requisito/guardar", fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function (response) {
            console.log(response.data);
            listarRequisitos();
            $('#modalEdit').modal('hide');
            $scope.reset();
        });
    }

    $scope.deleteSubmit = function () {

        console.log($scope.selectedRequisito);
        var fd = new FormData();
        fd.append("id", $scope.selectedRequisito.id);

        $http.post(url_root + "requisito/eliminar", fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function (response) {
            console.log(response.data);
            listarRequisitos();
            $('#modalDelete').modal('hide');
            $scope.reset();
        });
    }

    $scope.selectItem = function ($i_requisito) {
        $scope.selectedRequisito.id = $i_requisito.id;
        $scope.selectedRequisito.nombre = $i_requisito.nombre;
        $scope.selectedRequisito.descripcion = $i_requisito.descripcion;

    }

    $scope.reset = function () {
        $scope.newRequisito = {};
        $scope.selectedRequisito = {};
    }

    function prepararReporte() {
        $scope.fileName = "reporteRequisito";
        $scope.exportData = [];
        $scope.exportData.push(["id", "nombre", "descripcion"]);
        angular.forEach($scope.l_requisitos, function (m) {
            $scope.exportData.push([m.id, m.nombre, m.descripcion]);
        });
    }
});


app.directive('excelExport',
    function () {
        return {
            restrict: 'A',
            scope: {
                fileName: "@",
                data: "&exportData"
            },
            replace: true,
            template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()">Export to Excel <i class="fa fa-download"></i></button>',
            link: function (scope, element) {

                scope.download = function () {

                    function datenum(v, date1904) {
                        if (date1904) v += 1462;
                        var epoch = Date.parse(v);
                        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                    };

                    function getSheet(data, opts) {
                        var ws = {};
                        var range = {
                            s: {
                                c: 10000000,
                                r: 10000000
                            },
                            e: {
                                c: 0,
                                r: 0
                            }
                        };
                        for (var R = 0; R != data.length; ++R) {
                            for (var C = 0; C != data[R].length; ++C) {
                                if (range.s.r > R) range.s.r = R;
                                if (range.s.c > C) range.s.c = C;
                                if (range.e.r < R) range.e.r = R;
                                if (range.e.c < C) range.e.c = C;
                                var cell = {
                                    v: data[R][C]
                                };
                                if (cell.v == null) continue;
                                var cell_ref = XLSX.utils.encode_cell({
                                    c: C,
                                    r: R
                                });

                                if (typeof cell.v === 'number') cell.t = 'n';
                                else if (typeof cell.v === 'boolean') cell.t = 'b';
                                else if (cell.v instanceof Date) {
                                    cell.t = 'n';
                                    cell.z = XLSX.SSF._table[14];
                                    cell.v = datenum(cell.v);
                                } else cell.t = 's';

                                ws[cell_ref] = cell;
                            }
                        }
                        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                        return ws;
                    };

                    function Workbook() {
                        if (!(this instanceof Workbook)) return new Workbook();
                        this.SheetNames = [];
                        this.Sheets = {};
                    }

                    var wb = new Workbook(),
                        ws = getSheet(scope.data());
                    /* add worksheet to workbook */
                    wb.SheetNames.push(scope.fileName);
                    wb.Sheets[scope.fileName] = ws;
                    var wbout = XLSX.write(wb, {
                        bookType: 'xlsx',
                        bookSST: true,
                        type: 'binary'
                    });

                    function s2ab(s) {
                        var buf = new ArrayBuffer(s.length);
                        var view = new Uint8Array(buf);
                        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                        return buf;
                    }

                    saveAs(new Blob([s2ab(wbout)], {
                        type: "application/octet-stream"
                    }), scope.fileName + '.xlsx');

                };

            }
        };
    }
);