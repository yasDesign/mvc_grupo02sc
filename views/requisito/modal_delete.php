<div class="modal" id="modalDelete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Eliminar {{modelo}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form ng-submit="deleteSubmit()">
                <label for="">Estas seguro de Eliminar este {{modelo}} del sistema?</label>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary">Eliminar</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>