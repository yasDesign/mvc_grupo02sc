<div class="form-group">
    <input type="text" class="form-control" ng-model="searchRequisito">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title">Id </th>
                <th class="column-title">Fecha </th>
                <th class="column-title">Autoridad </th>
                <th class="column-title">Accion </th>
            </tr>
        </thead>

        <tbody>
            <tr class="even pointer" ng-repeat="i_requisito in l_requisitos | filter:searchRequisito |orderBy:'id'">
                <td class=" ">{{i_requisito.id}}</td>
                <td class=" ">{{i_requisito.nombre}}</td>
                <td class=" ">{{i_requisito.descripcion}} </td>
                <td>
                    <button class="btn btn-primary" type="reset" ng-click="selectItem(i_requisito)" data-target="#modalEdit" data-toggle="modal">Editar</button> 
                    <button class="btn btn-danger" type="reset" ng-click="selectItem(i_requisito)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div excel-export export-data="exportData" file-name="{{fileName}}"></div>