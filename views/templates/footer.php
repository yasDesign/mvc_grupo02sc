<?php require_once "main_containerFooter.php";?>

<!-- jQuery -->
<script src="<?php echo URL_ROOT;?>./assets/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo URL_ROOT;?>./assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo URL_ROOT;?>./assets/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo URL_ROOT;?>./assets/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo URL_ROOT;?>./assets/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?php echo URL_ROOT;?>./assets/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo URL_ROOT;?>./assets/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?php echo URL_ROOT;?>./assets/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="<?php echo URL_ROOT;?>./assets/skycons/skycons.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/validator/validator.js"></script>

<!-- Flot -->
<script src="<?php echo URL_ROOT;?>./assets/Flot/jquery.flot.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/Flot/jquery.flot.pie.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/Flot/jquery.flot.time.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/Flot/jquery.flot.stack.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?php echo URL_ROOT;?>./assets/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?php echo URL_ROOT;?>./assets/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="<?php echo URL_ROOT;?>./assets/jqvmap/dist/jquery.vmap.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo URL_ROOT;?>./assets/moment/min/moment.min.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo URL_ROOT;?>./assets/js/custom.min.js"></script>

<!-- Reportes... -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.1/jszip.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.1/xlsx.js"></script>
<script src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.js"></script>
</body>

</html>