
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Inegas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo URL_ROOT;?>./assets/landing/css/bootstrap.min.css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo URL_ROOT;?>./assets/landing/css/font-awesome.min.css">
    <!-- Simple Line Font -->
    <link rel="stylesheet" href="<?php echo URL_ROOT;?>./assets/landing/css/simple-line-icons.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo URL_ROOT;?>./assets/landing/css/owl.carousel.min.css">
    <!-- Main CSS -->
    <link href="<?php echo URL_ROOT;?>./assets/landing/css/style.css" rel="stylesheet">
</head>

<body>
<!-- ========================= ABOUT IMAGE =========================-->
<div class="about_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="index.html"><img src="<?php echo URL_ROOT;?>./assets/landing/images/responsive-logo.png" class="responsive-logo img-fluid" alt="responsive-logo"></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                        <span class="icon-menu"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo URL_ROOT;?>inicio/login">Home<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Admiciones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Programas</a>
                            </li>
                            <li class="nav-logo">
                                <a href="index.html" class="navbar-brand"><img src="<?php echo URL_ROOT;?>./assets/landing/images/logo.png" class="img-fluid" alt="logo"></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Cronograma</a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link" href="#">Blong</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo URL_ROOT;?>inicio/login">Login</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1>INEGAS</h1>
            </div>
        </div>
    </div>
</div>
<!--//END ABOUT IMAGE -->
<!--============================= ADMISSION COURCES =============================-->
<section class="admission_cources">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h2>Academica</h2>
<p>
    Somos una Institución Educativa creada con la finalidad de cubrir la necesidad de dotar a los recursos humanos, mayor saber, en todas las áreas donde se requiere instrucción, conocimientos, actualización, información y motivación especial.
</p>            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="course_filter">
                    <form>
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <select class="form-control custom-select">
                                    <option selected>Select Course</option>
                                    <option value="1">Course One</option>
                                    <option value="2">Course Two</option>
                                    <option value="3">Course Three</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-lg-2">
                                <select class="form-control custom-select">
                                    <option selected>Starts</option>
                                    <option value="1">in 3 months</option>
                                    <option value="2">in 6 months</option>
                                    <option value="3">more than 6 months</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-lg-2">
                                <select class="form-control custom-select">
                                    <option selected>Length</option>
                                    <option value="1">Less than 1 year</option>
                                    <option value="2">1 - 3 years</option>
                                    <option value="3">3 years and above</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-lg-2">
                                <select class="form-control custom-select">
                                    <option selected>Fee range</option>
                                    <option value="1">Less than $1000</option>
                                    <option value="2">$1000 to $3000</option>
                                    <option value="3">$3000 and above</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-lg-3">
                                <button class="btn btn-block btn-admission">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_01.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_02.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_03.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_04.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_05.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_06.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_07.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="course-detail.html" class="course_box">
                    <img src="<?php echo URL_ROOT;?>./assets/landing/images/admission/our-cources_08.jpg" class="img-fluid" alt="#">
                    <div class="couse-desc-wrap">
                        <h4>Course Name</h4>
                        <div class="star-rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <!-- // end .star-rating -->
                        <p>Lorem Ipsum is simply dummy text of the printing &amp; typesetting industry. Lorem Ipsum has.</p>
                    </div>
                    <!-- // end .couse-desc-wrap -->
                    <div class="course_duration">
                        <ul>
                            <li>
                                <p>Duration</p><span>1 Year</span></li>
                            <li>
                                <p>Fee</p><span>$500</span></li>
                            <li>
                                <p>Timing</p><span>10 am-2 pm</span></li>
                        </ul>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!--//END ADMISSION COURCES -->
<!--============================= Instagram Feed =============================-->
<div id="instafeed"></div>
<!--//END Instagram feed JS -->
<!--============================= FOOTER =============================-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="subscribe">
                    <h3>Newsletter</h3>
                    <form id="subscribeform" action="php/subscribe.php" method="post">
                        <input class="signup_form" type="text" name="email" placeholder="Enter Your Email Address">
                        <button type="submit" class="btn btn-warning" id="js-subscribe-btn">SUBSCRIBE</button>
                        <div id="js-subscribe-result" data-success-msg="Success, Please check your email." data-error-msg="Oops! Something went wrong"></div>
                        <!-- // end #js-subscribe-result -->
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="foot-logo">
                    <a href="index.html">
                        <img src="images/footer-logo.png" class="img-fluid" alt="footer_logo">
                    </a>
                    <p>Todos los derechos reservados © INEGAS
                        <br> All rights reserved.</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="sitemap">
                    <h3>Navigation</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tweet_box">
                    <h3>Tweets</h3>
                    <div class="tweet-wrap">
                        <div class="tweet"></div>
                        <!-- // end .tweet -->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="address">
                    <h3>Contactanos en</h3>
                    <p><span>Address: </span> Av Bush</p>
                    <p>Email : inegas@inegas.edu.bo
                        <br> Phone : +591-77074750 +591-3-3519061</p>
                    <ul class="footer-social-icons">
                        <li><a href="#"><i class="fa fa-facebook fa-fb" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin fa-in" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter fa-tw" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--//END FOOTER -->
<!-- jQuery, Bootstrap JS. -->
<script src="<?php echo URL_ROOT;?>./assets/landing/js/jquery.min.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/landing/js/tether.min.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/landing/js/bootstrap.min.js"></script>
<!-- Plugins -->
<script src="<?php echo URL_ROOT;?>./assets/landing/js/instafeed.min.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/landing/js/owl.carousel.min.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/landing/js/validate.js"></script>
<script src="<?php echo URL_ROOT;?>./assets/landing/js/tweetie.min.js"></script>
<!-- Subscribe -->
<script src="<?php echo URL_ROOT;?>./assets/landing/js/subscribe.js"></script>
<!-- Script JS -->
<script src="<?php echo URL_ROOT;?>./assets/landing/js/script.js"></script>
</body>

</html>
