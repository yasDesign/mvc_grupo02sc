<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li>
        <a href="#">
          <i class="fa fa-home"></i> Dashboard </a>
      </li>
      <li>
        <a>
          <i class="fa fa-users"></i> Usuarios
          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a  href="<?php echo URL_ROOT."administrador/admin";?>" >Administrador</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."estudiante/admin";?>">Estudiante</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."docente/admin";?>" >Docente</a>
          </li>
          <li>
            <a  href="<?php echo URL_ROOT."trasncriptor/admin";?>" >Transcriptor</a>
          </li>
        </ul>
      </li>
      <li>
        <a>
          <i class="fa fa-desktop"></i> Programas
          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a href="<?php echo URL_ROOT."titulacion/admin";?>">Titulacion</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."programa/admin";?>">Programas</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."modulo/admin";?>">Modulos</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."categoria/admin";?>">Categoria</a>
          </li>
        </ul>
      </li>
      <li>
        <a>
          <i class="fa fa-table"></i> Inscripciones
          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a href="<?php echo URL_ROOT."inscripcion/admin";?>">Inscripcion</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."convalidacion/admin";?>">Convalidacion</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."notas/admin";?>">Notas</a>
          </li>
          <li>
            <a href="<?php echo URL_ROOT."Pagos/admin";?>">Pago de Cuota</a>
          </li>
        </ul>
      </li>
      <li>
        <a>
          <i class="fa fa-bar-chart-o"></i> Reporte y Estadistica
          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a href="chartjs.html">Chart JS</a>
          </li>
          <li>
            <a href="chartjs2.html">Chart JS2</a>
          </li>
          <li>
            <a href="morisjs.html">Moris JS</a>
          </li>
          <li>
            <a href="echarts.html">ECharts</a>
          </li>
          <li>
            <a href="other_charts.html">Other Charts</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>

</div>