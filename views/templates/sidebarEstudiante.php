<!-- menu profile quick info -->
<div class="profile clearfix">
    <div class="profile_pic">
        <img src="<?php echo URL_ROOT;?>images/img.jpg" alt="..."class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span>Welcome,</span>
        <h2>
            <?php Sesion::init();echo Sesion::get('nombre');?>
        </h2>
    </div>
</div>
<!-- /menu profile quick info -->

<br />

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Estudiante...</h3>
        <ul class="nav side-menu">
            <li>
                <a href="<?php echo URL_ROOT."inicio/dashboard";?>">
                    <i class="fa fa-home"></i> Dashboard </a>
            </li>
            <li>
                <a>
                    <i class="fa fa-table"></i> Inscripciones
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li>
                        <a href="<?php echo URL_ROOT."inscripcion/admin";?>">Inscripcion</a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>

</div>