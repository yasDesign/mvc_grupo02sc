<div ng-controller="adminTitulacionController">

    <h2>{{modelo}}</h2>
    <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">Agregar Nuevo</button>

    <?php require_once ("modal_list.php");?>
    <?php require_once ("modal_add.php");?>
    <?php require_once ("modal_delete.php");?>
    <?php require_once ("modal_edit.php");?>

    <?php require_once ("modal_requisito.php");?>
    <?php require_once ("modal_programa.php");?>
     
</div>
<script src="<?php echo URL_ROOT."views/titulacion/js/adminTitulacionController.js";?>"></script>