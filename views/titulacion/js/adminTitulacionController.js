
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",['checklist-model']);

app.controller("adminTitulacionController",function($scope,$http){
    $scope.modelo="Plan de Estudios";
    $scope.l_titulaciones=[];
    $scope.l_categoria=[];
    $scope.user=[];
    $scope.singleSelect={};
    
    $scope.l_selectedRequisito=[];
    
    $scope.selectedTitulacion=[];

    listarTitulacion();
    listarCategoria();
    listarRequisito();

    function listarTitulacion(){
        $http.get(url_root+"titulacion/listarJSON").then(function(response){
            $scope.l_titulaciones=response.data;
            console.log(response.data);
        });
    }

    function listarCategoria(){
        $http.get(url_root+"categoria/listarJSON").then(function(response){
            $scope.l_categoria=response.data;
            console.log(response.data);
        });
    }

    $scope.selectItem=function(i_titulacion){
        $scope.selectedTitulacion.id=i_titulacion.id;
        $scope.selectedTitulacion.nombre=i_titulacion.nombre;
        $scope.selectedTitulacion.versioncap=i_titulacion.versioncap;
        $scope.selectedTitulacion.edicion=i_titulacion.edicion;
        $scope.selectedTitulacion.created=i_titulacion.created;
        $scope.selectedTitulacion.fechainicio=i_titulacion.fechainicio;
        $scope.selectedTitulacion.fechafin=i_titulacion.fechafin;
        $scope.selectedTitulacion.costo=i_titulacion.costo;
        $scope.selectedTitulacion.porcentaje=i_titulacion.porcentaje;
        $scope.selectedTitulacion.total=i_titulacion.total;
        $scope.selectedTitulacion.estado=i_titulacion.estado;
        $scope.selectedTitulacion.idtipo=i_titulacion.idtipo;
        $scope.selectedTitulacion.idaprovacion=i_titulacion.idaprovacion;
        $scope.selectedTitulacion.codigo=i_titulacion.codigo;
    }

    $scope.selectItem2=function(i_titulacion,tipo){
        $scope.selectedTitulacion.id=i_titulacion.id;
        if(tipo==1){
            programaLoad();
        }
        if(tipo==2){
            requisitoLoad();
        }
    }

    $scope.addSubmit=function(){
        console.log($scope.newTitulacion);
        var fd=new FormData();
        fd.append("nombre",$scope.newTitulacion.nombre);
        fd.append("versioncap",$scope.newTitulacion.versioncap);
        fd.append("edicion",$scope.newTitulacion.edicion);
        fd.append("fechainicio",$scope.newTitulacion.fechainicio);
        fd.append("fechafin",$scope.newTitulacion.fechafin);
        fd.append("costo",$scope.newTitulacion.costo);
        fd.append("porcentaje",$scope.newTitulacion.porcentaje);
        fd.append("total",$scope.newTitulacion.total);
        fd.append("estado",$scope.newTitulacion.estado);
        fd.append("idtipo",$scope.newTitulacion.idtipo);
        fd.append("idaprovacion",$scope.newTitulacion.idaprovacion);
        fd.append("codigo",$scope.newTitulacion.codigo);        
        
        $http.post(url_root+"titulacion/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
            }).then(function(response){
                console.log(response.data);
                listarTitulacion();
                $('#modalAdd').modal('hide');
                $scope.reset();
            });
    }


    $scope.editSubmit=function(){
        console.log($scope.selectedTitulacion);
        var fd=new FormData();
        fd.append("id",$scope.selectedTitulacion.id);
        fd.append("nombre",$scope.selectedTitulacion.nombre);
        fd.append("versioncap",$scope.selectedTitulacion.versioncap);
        fd.append("edicion",$scope.selectedTitulacion.edicion);
        fd.append("fechainicio",$scope.selectedTitulacion.fechainicio);
        fd.append("fechafin",$scope.selectedTitulacion.fechafin);
        fd.append("costo",$scope.selectedTitulacion.costo);
        fd.append("porcentaje",$scope.selectedTitulacion.porcentaje);
        fd.append("total",$scope.selectedTitulacion.total);
        fd.append("estado",$scope.selectedTitulacion.estado);
        fd.append("idtipo",$scope.selectedTitulacion.idtipo);
        fd.append("idaprovacion",$scope.selectedTitulacion.idaprovacion);
        fd.append("codigo",$scope.selectedTitulacion.codigo);        
        
        $http.post(url_root+"titulacion/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
            }).then(function(response){
                console.log(response.data);
                listarTitulacion();
                $('#modalEdit').modal('hide');
                $scope.reset();
            });
    }

    $scope.deleteSubmit=function(){
        
        console.log($scope.selectedTitulacion.id);
        var fd=new FormData();
        fd.append("id",$scope.selectedTitulacion.id);
        
        $http.post(url_root+"titulacion/eliminar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
                console.log(response.data);
                listarTitulacion();
                $('#modalDelete').modal('hide');
                $scope.reset();
        });
    }

    $scope.reset=function(){
        $scope.selectedTitulacion={};
        $scope.newTitulacion={};
    }

    $scope.getCategoria=function(id){
       var len=$scope.l_categoria.length;
       for (i=0; i<len; i++){
            if($scope.l_categoria[i].id==id){
                return $scope.l_categoria[i].nombre;
            }
       }
    };

    $scope.seleccionarCapa=function(){
        console.log($scope.singleSelect);
        $scope.l_selectedRequisito.push($scope.singleSelect);
        $('#addCapacitacion').modal('hide');
    }

    //*************************************//
    $scope.user = {
        programa: [],
        requisito:[]
      };

    ///////////////////////////////////////7
    /*listarmodalidadPago();
    $scope.l_modalidadPago=[];
    function listarmodalidadPago(){
        $http.get(url_root+"modalidadPago/listarJSON").then(function(response){
            $scope.l_modalidadPago=response.data;
            console.log(response.data);
        });
    }*/


    ///////////////////////////////////////7
    listarRequisito();
    $scope.l_requisito=[];
    function listarRequisito(){
        $http.get(url_root+"requisito/listarJSON").then(function(response){
            $scope.l_requisito=response.data;
            console.log(response.data);
        });
    }  

    //////////////////////////////////////7
    listarPrograma();
    $scope.l_programa=[];
    function listarPrograma(){
        $http.get(url_root+"programa/listarJSON").then(function(response){
            $scope.l_programa=response.data;
            console.log(response.data);
        });
    }
    
    //************************************* */
    function programaLoad(){
               var fd=new FormData();    
                fd.append("idtitulo",$scope.selectedTitulacion.id);
                $http.post(url_root+"titulacion/listarPrograma",fd,{
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined,
                        'Process-Data': false
                    }
                }).then(function(response){
                        console.log(response.data);
                        //listarTitulacion();
                        //$('#modalPrograma').modal('hide');
                        angular.forEach(response.data,function(item){
                            $scope.user.programa.push(item.idprograma);
                        });
                        //$scope.user.programa=response.data;
                        
                });
    }

    function requisitoLoad(){
        var fd=new FormData();    
         fd.append("idtitulo",$scope.selectedTitulacion.id);
         $http.post(url_root+"titulacion/listarRequisito",fd,{
             transformRequest: angular.identity,
             headers: {
                 'Content-Type': undefined,
                 'Process-Data': false
             }
         }).then(function(response){
                 console.log(response.data);
                 //listarTitulacion();
                 //$('#modalPrograma').modal('hide');
                 angular.forEach(response.data,function(item){
                     $scope.user.requisito.push(item.idrequisito);
                 });
                 //$scope.user.programa=response.data;
                 
         });
    }   

    $scope.programaSubmit=function(){
        if($scope.user.programa.length>0){
            console.log("ok");
            console.log($scope.user.programa);
            console.log($scope.selectedTitulacion.id);
            
            angular.forEach($scope.user.programa,function(item){
                var fd=new FormData();    
                fd.append("idtitulo",$scope.selectedTitulacion.id);
                fd.append("idprograma",item);
                $http.post(url_root+"titulacion/guardarPrograma",fd,{
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined,
                        'Process-Data': false
                    }
                }).then(function(response){
                        console.log(response.data);
                        //listarTitulacion();
                        $('#modalPrograma').modal('hide');
                        $scope.user.programa=[];
                });
            });
        }else{
            console.log("not ok");
        }
    }

    $scope.requisitoSubmit=function(){
        if($scope.user.requisito.length>0){
            console.log("ok");
            console.log($scope.user.requisito);
            console.log($scope.selectedTitulacion.id);
            
            angular.forEach($scope.user.requisito,function(item){
                var fd=new FormData();    
                fd.append("idtitulo",$scope.selectedTitulacion.id);
                fd.append("idrequisito",item);
                $http.post(url_root+"titulacion/guardarRequisito",fd,{
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined,
                        'Process-Data': false
                    }
                }).then(function(response){
                        console.log(response.data);
                        //listarTitulacion();
                        $('#modalRequisito').modal('hide');
                        $scope.user.requisito=[];
                });
            });
        }else{
            console.log("not ok");
        }
    }
    
      
      /*$scope.checkAll = function() {
        $scope.user.programa = $scope.programa.map(function(item) { return item.id; });
      };
      $scope.uncheckAll = function() {
        $scope.user.programa = [];
      };
      $scope.checkFirst = function() {
        $scope.user.programa.splice(0, $scope.user.programa.length); 
        $scope.user.programa.push(1);
      };*/
});
