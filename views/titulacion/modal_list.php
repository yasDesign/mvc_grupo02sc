<div class="form-group">
    <input type="text" class="form-control" ng-model="searchTitulacion">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title">Id </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">fechainicio </th>
                <th class="column-title">fechafin </th>
                <th class="column-title">costo </th>
                <th class="column-title">Categoria </th>                
                <th>Accion</th>
                <th>Configuracion</th>
            </tr>
        </thead>

        <tbody>
            <tr class="even pointer" ng-repeat="i_titulacion in l_titulaciones | orderBy:'id' | filter:searchTitulacion">
                <td>{{i_titulacion.id}}</td>
                <td>{{i_titulacion.nombre}}</td>
                <td>{{i_titulacion.fechainicio}} </td>
                <td>{{i_titulacion.fechafin}} </td>
                <td>{{i_titulacion.costo}} </td>
                <td>{{getCategoria(i_titulacion.idtipo)}}</td>
                <td>
                    <button class="btn btn-primary" type="reset" ng-click="selectItem(i_titulacion)" data-target="#modalEdit" data-toggle="modal">Editar</button>
                    <button class="btn btn-danger" type="reset" ng-click="selectItem(i_titulacion)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
                </td>
                <td>
                    <button class="btn btn-info" type="reset" ng-click="selectItem2(i_titulacion,2)" data-target="#modalRequisito" data-toggle="modal">Requisitos</button>
                    <button class="btn btn-info" type="reset" ng-click="selectItem(i_titulacion)" data-target="#modalPago" data-toggle="modal">Modalidad pagos</button>
                    <button class="btn btn-info" type="reset" ng-click="selectItem2(i_titulacion,1)" data-target="#modalPrograma" data-toggle="modal">Programas</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>