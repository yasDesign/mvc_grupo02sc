<div class="modal" id="modalPrograma">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modificacion de {{ modelo }}</h4>
            </div>
            <div class="modal-body">
                

                    <div class="form-group">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12 hide" ng-model="selectedTitulacion.id">
                    </div>

                    <div class="form-group">
                        <label>Programa (1 minimum):</label>
                        <p style="padding: 5px;" ng-repeat="i_programa in l_programa">
                            <input type="checkbox" class="flat"  checklist-model="user.programa" checklist-value="i_programa.id"/> {{i_programa.nombre}}
                            <br />    
                        <p>
                        {{user}}
                    </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success" ng-click="programaSubmit()">Submit</button>
                
                <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary" type="reset">Reset</button>
            </div>
        </div>
    </div>

</div>