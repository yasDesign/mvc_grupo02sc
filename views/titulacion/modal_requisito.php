<div class="modal" id="modalRequisito">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Asignacion de Requisitos</h4>
            </div>
            <div class="modal-body">

                    <div class="form-group">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12 hide" ng-model="selectedTitulacion.id">
                    </div>
                    
                    <div class="form-group">
                        <label>Requisito (1 minimum):</label>
                        <p style="padding: 5px;" ng-repeat="i_requisito in l_requisito">
                            <input type="checkbox" class="flat"  checklist-model="user.requisito" checklist-value="i_requisito.id"/> {{i_requisito.nombre}}
                            <br />    
                        <p>
                        {{user}}
                    </div>

            </div>
            <div class="modal-footer">
                <button ng-click="requisitoSubmit()" class="btn btn-success">Submit</button>
                <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary" type="reset">Reset</button>
            </div>
        </div>
    </div>

</div>