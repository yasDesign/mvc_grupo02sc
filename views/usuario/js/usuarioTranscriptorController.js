
var url_root="http://localhost/mvc_grupo02sc/";
var url_root="http://localhost/mvc_grupo02sc/";

var app=angular.module("app",[]);

app.controller("usuarioTranscriptorController",function($scope,$http){

    $scope.modelo="Transcriptor";

    $scope.l_usuarios=[];
    $scope.selectedUsuario=[];
    listarUsuarios();

    function listarUsuarios(){
        $http.get(url_root+"usuario/listarUsuarioJSON/?tipo=2").then(function(response){
            $scope.l_usuarios=response.data;
            console.log(response.data);
            prepararReporte();
        });
    }

    $scope.addSubmit=function(){
        console.log("ver antes de guardar");
        console.log($scope.newUsuario);
        var fd=new FormData();
        fd.append("nombre",$scope.newUsuario.nombre);
        fd.append("apellido",$scope.newUsuario.apellido);
        fd.append("nacionalidad",$scope.newUsuario.nacionalidad);
        fd.append("fechanacimiento",$scope.newUsuario.fechanacimiento);
        fd.append("lugarnacimiento",$scope.newUsuario.lugarnacimiento);
        fd.append("cedula",$scope.newUsuario.cedula);
        fd.append("calle",$scope.newUsuario.calle);
        fd.append("ciudad",$scope.newUsuario.ciudad);
        fd.append("telefono",$scope.newUsuario.telefono);
        fd.append("correo",$scope.newUsuario.correo);
        fd.append("contrasena",$scope.newUsuario.contrasena);
        fd.append("sexo",$scope.newUsuario.sexo);
        fd.append("tipo",2);


        $http.post(url_root+"usuario/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarUsuarios();
            $('#modalAdd').modal('hide');
            $scope.reset();
        });
    }

    $scope.editSubmit=function(){

        console.log($scope.selectedUsuario);
        var fd=new FormData();
        fd.append("id",$scope.selectedUsuario.id);
        fd.append("nombre",$scope.selectedUsuario.apellido);
        fd.append("apellido",$scope.selectedUsuario.apellido);
        fd.append("nacionalidad",$scope.selectedUsuario.nacionalidad);
        fd.append("fechanacimiento",$scope.selectedUsuario.fechanacimiento);
        fd.append("lugarnacimiento",$scope.selectedUsuario.lugarnacimiento);
        fd.append("cedula",$scope.selectedUsuario.cedula);
        fd.append("calle",$scope.selectedUsuario.calle);
        fd.append("ciudad",$scope.selectedUsuario.ciudad);
        fd.append("telefono",$scope.selectedUsuario.telefono);
        fd.append("correo",$scope.selectedUsuario.correo);
        fd.append("contrasena",$scope.selectedUsuario.contrasena);
        fd.append("sexo",$scope.selectedUsuario.sexo);
        fd.append("tipo",2);

        $http.post(url_root+"usuario/guardar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarUsuarios();
            $('#modalEdit').modal('hide');
            $scope.reset();
        });
    }

    $scope.deleteSubmit=function(){

        console.log($scope.selectedUsuario);
        var fd=new FormData();
        fd.append("id",$scope.selectedUsuario.id);

        $http.post(url_root+"usuario/eliminar",fd,{
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        }).then(function(response){
            console.log(response.data);
            listarUsuarios();
            $('#modalDelete').modal('hide');
            $scope.reset();
        });
    }


    $scope.selectItem=function($i_usuario){
        $scope.selectedUsuario.id=$i_usuario.id;
        $scope.selectedUsuario.nombre=$i_usuario.nombre;
        $scope.selectedUsuario.apellido=$i_usuario.apellido;
        $scope.selectedUsuario.nacionalidad=$i_usuario.nacionalidad;
        $scope.selectedUsuario.fechanacimiento=$i_usuario.fechanacimiento;
        $scope.selectedUsuario.lugarnacimiento=$i_usuario.lugarnacimiento;
        $scope.selectedUsuario.cedula=$i_usuario.cedula;
        $scope.selectedUsuario.calle=$i_usuario.calle;
        $scope.selectedUsuario.ciudad=$i_usuario.ciudad;
        $scope.selectedUsuario.telefono=$i_usuario.telefono;
        $scope.selectedUsuario.correo=$i_usuario.correo;
        $scope.selectedUsuario.contrasena=$i_usuario.contrasena;
        $scope.selectedUsuario.sexo=$i_usuario.sexo;
    }

    $scope.reset=function(){
        $scope.newUsuario={};
        $scope.selectedUsuario={};
    }



    function prepararReporte() {
        $scope.fileName = "reporteUsuariosTranscriptor";
        $scope.exportData = [];
        $scope.exportData.push(["id", "nombre", "apellido","nacionalidad","fechanacimiento","lugarnacimiento","cedula","calle","ciudad","telefono","correo","contrasena","sexo"]);
        angular.forEach($scope.l_usuarios, function (m) {
            $scope.exportData.push([m.id, m.nombre, m.apellido,m.nacionalidad,m.fechanacimiento,m.lugarnacimiento,m.cedula,m.calle,m.ciudad,m.telefono,m.correo,m.contrasena,m.sexo]);
        });
    }
});


app.directive('excelExport',
    function () {
        return {
            restrict: 'A',
            scope: {
                fileName: "@",
                data: "&exportData"
            },
            replace: true,
            template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()">Export to Excel <i class="fa fa-download"></i></button>',
            link: function (scope, element) {

                scope.download = function () {

                    function datenum(v, date1904) {
                        if (date1904) v += 1462;
                        var epoch = Date.parse(v);
                        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                    };

                    function getSheet(data, opts) {
                        var ws = {};
                        var range = {
                            s: {
                                c: 10000000,
                                r: 10000000
                            },
                            e: {
                                c: 0,
                                r: 0
                            }
                        };
                        for (var R = 0; R != data.length; ++R) {
                            for (var C = 0; C != data[R].length; ++C) {
                                if (range.s.r > R) range.s.r = R;
                                if (range.s.c > C) range.s.c = C;
                                if (range.e.r < R) range.e.r = R;
                                if (range.e.c < C) range.e.c = C;
                                var cell = {
                                    v: data[R][C]
                                };
                                if (cell.v == null) continue;
                                var cell_ref = XLSX.utils.encode_cell({
                                    c: C,
                                    r: R
                                });

                                if (typeof cell.v === 'number') cell.t = 'n';
                                else if (typeof cell.v === 'boolean') cell.t = 'b';
                                else if (cell.v instanceof Date) {
                                    cell.t = 'n';
                                    cell.z = XLSX.SSF._table[14];
                                    cell.v = datenum(cell.v);
                                } else cell.t = 's';

                                ws[cell_ref] = cell;
                            }
                        }
                        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                        return ws;
                    };

                    function Workbook() {
                        if (!(this instanceof Workbook)) return new Workbook();
                        this.SheetNames = [];
                        this.Sheets = {};
                    }

                    var wb = new Workbook(),
                        ws = getSheet(scope.data());
                    /* add worksheet to workbook */
                    wb.SheetNames.push(scope.fileName);
                    wb.Sheets[scope.fileName] = ws;
                    var wbout = XLSX.write(wb, {
                        bookType: 'xlsx',
                        bookSST: true,
                        type: 'binary'
                    });

                    function s2ab(s) {
                        var buf = new ArrayBuffer(s.length);
                        var view = new Uint8Array(buf);
                        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                        return buf;
                    }

                    saveAs(new Blob([s2ab(wbout)], {
                        type: "application/octet-stream"
                    }), scope.fileName + '.xlsx');

                };

            }
        };
    }
);