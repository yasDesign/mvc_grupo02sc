<div class="form-group">
    <input type="text" class="form-control" ng-model="searchUsuario">
</div>
<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title">Codigo </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">apellido </th>
                <th class="column-title">nacionalidad </th>
                <th class="column-title">telefono </th>
                <th class="column-title">sexo </th>
                <th class="column-title">correo </th>
                <th class="column-title">contrasena </th>
                <th class="column-title">Accion </th>

            </tr>
        </thead>

        <tbody>
            <tr class="even pointer" ng-repeat="i_usuario in l_usuarios |filter:searchUsuario |orderBy:'codigo'">
                <td class=" ">{{i_usuario.codigo}}</td>
                <td class=" ">{{i_usuario.nombre}}</td>
                <td class=" ">{{i_usuario.apellido}} </td>
                <td class=" ">{{i_usuario.nacionalidad}} </td>
                <td class=" ">{{i_usuario.telefono}} </td>
                <td class=" ">{{i_usuario.sexo}} </td>
                <td class=" ">{{i_usuario.correo}} </td>
                <td class=" ">{{i_usuario.contrasena}} </td>
                <td>
                    <button class="btn btn-primary" type="reset" ng-click="selectItem(i_usuario)" data-target="#modalEdit" data-toggle="modal">Editar</button>
                    <button class="btn btn-danger" type="reset" ng-click="selectItem(i_usuario)" data-target="#modalDelete" data-toggle="modal">Eliminar</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div excel-export export-data="exportData" file-name="{{fileName}}"></div>